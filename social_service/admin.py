from django.contrib import admin
from accounts.models import City
from social_service.models import Post, Category, SubCategory


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'created_at']
    list_filter = ['author']
    search_fields = ['title', 'body']
    fields = ['title', 'body', 'author', 'created_at', 'updated_at']
    readonly_fields = ['created_at', 'updated_at']

class CityAdmin(admin.ModelAdmin):
    fields = ['name']


class SubCategoryInline(admin.TabularInline):
    model = SubCategory
    extra = 1

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    inlines = [SubCategoryInline]

admin.site.register(SubCategory)


admin.site.register(City, CityAdmin)
admin.site.register(Post, PostAdmin)
