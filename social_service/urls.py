from django.urls import path
from .views import create_post, PostListView, PostDeleteView, PostUpdateView, PostDetailView, post_filter



urlpatterns = [
    path('<int:pk>/detail/', PostDetailView.as_view(), name='detail'),
    path('create/', create_post, name='create_post'),
    path('filter/', post_filter, name='post_filter'),
    path('list/', PostListView.as_view(), name='list'),
    path('<int:pk>/update/', PostUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', PostDeleteView.as_view(), name='delete'),

    # path('<int:post_pk>/detail/comment/create/', CommentCreateView.as_view(), name='comment_create'),
    # path('<int:post_pk>/detail/comment/update/', CommentUpdateView.as_view(), name='comment_update'),
    # path('<int:post_pk>/detail/comment/delete/', CommentDeleteView.as_view(), name='comment_delete'),
]