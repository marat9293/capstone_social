from django import forms
from .models import Post, Comment, Category, SubCategory
from accounts.models import City

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'description', 'image', 'category', 'subcategory', 'city']

    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        # Добавляем CSS классы для стилизации полей, если нужно
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['image'].widget.attrs.update({'class': 'form-control-file'})
        self.fields['category'].widget.attrs.update({'class': 'form-control'})
        self.fields['subcategory'].widget.attrs.update({'class': 'form-control'})
        self.fields['city'].widget.attrs.update({'class': 'form-control'})


class PostFilterForm(forms.Form):
    city = forms.ModelChoiceField(queryset=City.objects.all(), required=False, label='Город')
    category = forms.ModelChoiceField(queryset=Category.objects.all(), required=False, label='Категория')
    subcategory = forms.ModelChoiceField(queryset=SubCategory.objects.all(), required=False, label='Подкатегория')

    def __init__(self, *args, **kwargs):
        super(PostFilterForm, self).__init__(*args, **kwargs)
        # Добавляем CSS классы для стилизации полей
        self.fields['city'].widget.attrs.update({'class': 'form-control'})
        self.fields['category'].widget.attrs.update({'class': 'form-control'})
        self.fields['subcategory'].widget.attrs.update({'class': 'form-control'})

# class PostForm(forms.ModelForm):
#     class Meta:
#         model = Post
#         fields = ['title', 'description', 'city', 'image']
#         widgets = {
#             'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Название'}),
#             'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4, 'placeholder': 'Описание'}),
#             'city': forms.Select(attrs={'class': 'form-select', 'placeholder': 'Город'}),
#             'image': forms.ClearableFileInput(attrs={'class': 'form-control'})
#         }

#     def clean(self):
#         cleaned_data = super().clean()
#         title = cleaned_data.get('title')
#         description = cleaned_data.get('description')

#         if title and description and title == description:
#             raise forms.ValidationError("Title and description should not be identical.")

#         return cleaned_data
    

class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={
                'class': 'form-control'}),
            'author': forms.TextInput(attrs={
                'class': 'form-control'})
        }

