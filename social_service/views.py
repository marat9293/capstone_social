from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from django.shortcuts import render, redirect
from .forms import PostForm, SearchForm
from django.contrib.auth.decorators import login_required
from .models import Post
from django.db.models import Q
from django.utils.http import urlencode
from django.urls import reverse, reverse_lazy
from .models import Post
from accounts.models import City
from .forms import PostFilterForm


@login_required
def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('home_page')
    else:
        form = PostForm()
    return render(request, 'posts/create_post.html', {'form': form})

# @login_required
# def create_post(request):
#     if request.method == 'POST':
#         form = PostForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = PostForm()
#     return render(request, 'create_post.html', {'form': form})

# def post_list(request):
#     posts = Post.objects.all()
#     return render(request, 'posts/posts_lists.html', {'posts': posts})


class PostListView(ListView):
    template_name = 'posts/post_list.html'
    model = Post
    context_object_name = 'posts'
    form = SearchForm
    paginate_by = 15
    paginate_orphans = 1
    search_value = None
    
    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)
    
    def get_search_form(self):
        return self.form(self.request.GET)
    
    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context
    
    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(title__icontains=self.search_value) | Q(author__name__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset
    

class PostDeleteView(DeleteView):
    template_name = 'posts/delete_post.html'
    model = Post
    context_object_name = 'post'
    success_url = reverse_lazy('list')


class PostUpdateView(UpdateView):
    model = Post
    template_name = 'posts/update_post.html'
    form_class = PostForm
    context_object_name = 'post'
    # permission_required = ['social_service.change_post', 'Social_service.can_read_post']


    def get_success_url(self):
        return reverse('detail', kwargs={'pk': self.object.pk})
    


class PostDetailView(DetailView):
    template_name = 'posts/post_detail.html'
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # comment_form = CommentForm()
        my_post = self.object
        # comments = my_post.comments.order_by('-created_at')
        # context['comments'] = comments
        # context['comment_form'] = comment_form
        return context

def post_filter(request):
    form = PostFilterForm(request.GET)
    posts = Post.objects.all()

    if form.is_valid():
        city = form.cleaned_data.get('city')
        category = form.cleaned_data.get('category')
        subcategory = form.cleaned_data.get('subcategory')

        if city:
            posts = posts.filter(city=city)
        if category:
            posts = posts.filter(category=category)
        if subcategory:
            posts = posts.filter(subcategory=subcategory)

    return render(request, 'posts/post_filter.html', {'form': form, 'posts': posts})



