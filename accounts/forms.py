from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Profile, BusinessProfile, City

class MyUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'username': 'Логин',
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'email': 'email',
            'password1': 'Пароль',
            'password2': 'Подтвердить пароль',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in ['password1', 'password2']:
            self.fields[field_name].widget.attrs.update({'class': 'form-control'})



class ProfileForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset=City.objects.all(), empty_label=None)
    class Meta:
        model = Profile
        exclude = ['user']
        widgets = {
            'birth_date': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'avatar': forms.FileInput(attrs={'class': 'form-control'}),
            'city': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'birth_date': 'Дата рождения',
            'city': 'Ваш город',
            'avatar': 'Ваше фото'
        }

    def clean_birth_date(self):
        birth_date = self.cleaned_data.get('birth_date')
        if not birth_date:
            raise forms.ValidationError("Поле 'Дата рождения' обязательно для заполнения")
        return birth_date

class BusinessProfileForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset=City.objects.all(), empty_label=None)

    class Meta:
        model = BusinessProfile 
        exclude = ['user']
        widgets = {
            'company_name': forms.TextInput(attrs={'class': 'form-control'}), 
            'logo': forms.FileInput(attrs={'class': 'form-control'}), 
            'city': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'company_name': 'Название компании', 
            'logo': 'Логотип', 
            'city': 'Город',
        }



    
