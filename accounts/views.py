from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.views.generic import CreateView, DetailView, View
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.core.paginator import Paginator
from .forms import MyUserCreationForm, ProfileForm, BusinessProfileForm
from .models import Profile, BusinessProfile


class RegisterView(CreateView):
    model = User
    form_class = MyUserCreationForm
    template_name = 'accounts/register.html'
    context_object_name = 'user_obj'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile_form'] = ProfileForm() 
        return context

    def form_valid(self, form):
        user = form.save()
        birth_date =self.request.POST.get('birth_date')
        city_id = self.request.POST.get('city')
        avatar = self.request.FILES.get('avatar')
        Profile.objects.create(
            user=user,
            birth_date=birth_date,
            city_id=city_id,
            avatar=avatar
            )
        login(self.request, user)
        return redirect(self.get_success_url())
    
    def get_success_url(self):
        next = self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('next')
        return next
    

class BusinessRegisterView(CreateView):
    model = User
    form_class = MyUserCreationForm
    template_name = 'accounts/register_busines.html'
    context_object_name = 'user_obj'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile_form'] = BusinessProfileForm() 
        return context

    def form_valid(self, form):
        user = form.save()
        company_name = self.request.POST.get('company_name')
        city_id = self.request.POST.get('city')  # Предполагается, что в форме city передаётся id города
        logo = self.request.FILES.get('logo')  # Получаем изображение из FILES
        BusinessProfile.objects.create(
            user=user,
            company_name=company_name,
            city_id=city_id,
            logo=logo
        )
        login(self.request, user)
        return redirect(self.get_success_url())
    
    def get_success_url(self):
        next = self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('next')
        return next








class LoginView(View):
    def get(self, request, *args, **kwargs):
        return render(request=request, template_name='accounts/login.html')

    def post(self, request, *args, **kwargs):
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('home_page')
        else:
            context['has_error'] = True
        return render(request=request, template_name='accounts/login.html', context=context)


def choice_view(request):
    return render(request, 'accounts/choice.html')


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        next = request.GET.get('next')
        if next:
            return redirect(next)
        return redirect('login')




class UserDetailView(DetailView):
    model = get_user_model()
    template_name = 'accounts/user_detail.html'
    context_object_name = 'user_obj'
    paginate_related_by = 15
    paginate_related_orphans = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Проверяем наличие профиля BusinessProfile и получаем URL логотипа
        if hasattr(self.object, 'businessprofile') and self.object.businessprofile.logo:
            logo_url = self.object.businessprofile.logo.url
        else:
            logo_url = None

        # Добавляем URL логотипа в контекст
        context['logo_url'] = logo_url

        # Получаем посты пользователя и добавляем их в контекст
        user_posts = self.object.posts.all()
        paginator = Paginator(user_posts, self.paginate_related_by, orphans=self.paginate_related_orphans)
        page_number = self.request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        context['user_posts'] = page

        return context
    


