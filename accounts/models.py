from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


class City(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    
class Profile(models.Model):
    user = models.OneToOneField(
        get_user_model(),
        related_name='profile',
        on_delete=models.CASCADE,
        verbose_name='User'
        )
    birth_date = models.DateField(null=True, blank=True, verbose_name='Birth date')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Avatar')
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, verbose_name='City')

    def __str__(self):
        return f"Профиль" + self.user.get_full_name() 


class BusinessProfile(models.Model):
    user = models.OneToOneField(
        get_user_model(),
        related_name='business_profile',
        on_delete=models.CASCADE,
        verbose_name='User'
    )
    company_name = models.CharField(max_length=50, blank=False)
    logo = models.ImageField(null=True, blank=True, upload_to='logos', verbose_name='Logo')
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, verbose_name='City')
    business_profile = models.BooleanField(default=True)

    def __str__(self):
        return f"Профиль компании {self.company_name}" 
