from django.urls import path
from .views import LoginView, LogoutView, choice_view, UserDetailView, RegisterView, BusinessRegisterView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('choice/', choice_view, name='choice'),
    path('register/', RegisterView.as_view(), name='register'),
    path('business_register', BusinessRegisterView.as_view(), name='business_register'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('<int:pk>/', UserDetailView.as_view(), name='user_detail'),
]
